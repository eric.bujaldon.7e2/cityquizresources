package cat.itb.cityquiz.presentation.startscreen;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class StartScreenViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    MutableLiveData<Game> gameMutableLiveData = new MutableLiveData<>();
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();

    public void createGame() {
        Game game = gameLogic.createGame(Game.maxQuestions, Game.possibleAnswers);
        gameMutableLiveData.postValue(game);
    }

    public MutableLiveData<Game> getGameMutableLiveData() {
        return gameMutableLiveData;
    }

    public void questionAnswered(int answer) {
        Game game = gameLogic.answerQuestions(gameMutableLiveData.getValue(),answer);
        gameMutableLiveData.postValue(game);
    }

    public boolean isFinished() {
        Game game = gameMutableLiveData.getValue();
        return game.isFinished();
    }
}
