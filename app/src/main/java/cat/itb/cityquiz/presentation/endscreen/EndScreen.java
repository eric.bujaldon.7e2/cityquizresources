package cat.itb.cityquiz.presentation.endscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.presentation.startscreen.StartScreenViewModel;

public class EndScreen extends Fragment {

    private StartScreenViewModel mViewModel;

    public static EndScreen newInstance() {
        return new EndScreen();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.end_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StartScreenViewModel.class);
        // TODO: Use the ViewModel
        mViewModel.getGameMutableLiveData().observe(this, this::onGameChanged);
//       display();
    }

    private void onGameChanged(Game game) {
        if (game != null && !game.isFinished()) {
            Navigation.findNavController(getView()).navigate(R.id.go_endscreen);
        }
    }
//    private void display() {
//        score.setText(String.valueOf(mViewModel.getGameMutableLiveData().getValue().getNumCorrectAnswers()));
//    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }
    @OnClick(R.id.PlayAgain)
    public void onViewClicked() {
        mViewModel.createGame();
    }
}
