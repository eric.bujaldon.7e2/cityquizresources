package cat.itb.cityquiz.presentation;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import cat.itb.cityquiz.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }
}

