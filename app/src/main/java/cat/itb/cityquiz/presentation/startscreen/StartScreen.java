package cat.itb.cityquiz.presentation.startscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class StartScreen extends Fragment {

    private StartScreenViewModel mViewModel;

    public static StartScreen newInstance() {
        return new StartScreen();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StartScreenViewModel.class);
        mViewModel.getGameMutableLiveData().observe(this, this::onGameChanged);
        // TODO: Use the ViewModel
    }
    private void onGameChanged(Game game) {
        if(game!=null){
            Navigation.findNavController(getView()).navigate(R.id.go_midscreen);
        }
    }
    @OnClick(R.id.StartButton)
    public void onViewClicked() {
        mViewModel.createGame();
    }
}
